package com.hendisantika.grpc.springbootgrpcexample;

import com.hendisantika.grpc.springbootgrpcexample.grpc.HelloWorldClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootGrpcExampleApplicationTests {

    @Autowired
    private HelloWorldClient helloWorldClient;

    @Test
    public void testSayHello() {
        assertThat(helloWorldClient.sayHello("Uzumaki", "Naruto")).isEqualTo("Hello Uzumaki Naruto!");
    }

}
